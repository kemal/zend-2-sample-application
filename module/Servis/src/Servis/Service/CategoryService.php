<?php

namespace Servis\Service; 

class CategoryService  extends AbstractService {

    protected $servisTable;
    protected $categoryTable;

    public function getServisTable() {
        if (!$this->servisTable) {
            $sm = $this->getServiceLocator();
            $this->servisTable = $sm->get('Servis\Model\ServisTable');
        }
        return $this->servisTable;
    }

    public function getCategoryTable() {
        if (!$this->categoryTable) {
            $sm = $this->getServiceLocator();
            $this->categoryTable = $sm->get('Servis\Model\CategoryTable');
        }
        return $this->categoryTable;
    }

    public function getCategoriesWithAlbums() {

        $zones = array();
        $categories = $this->getCategoryTable()->fetchAll();
        foreach ($categories as $category) {
            $albums = $this->getServisTable()->fetchWithCatId($category->id);

            $zones[] = array(
                'category' => $category, //array('id' => $category->id, 'name' => $category->name),
                'albums' => $albums
            );
        }
        return $zones;
    }

}
