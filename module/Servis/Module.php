<?php
namespace Servis;
// Add these import statements:
use Servis\Model\Servis;
use Servis\Model\ServisTable;

use Servis\Model\Category;
use Servis\Model\CategoryTable;
use Servis\Service\CategoryService;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module {

    // Add this method:
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Servis\Model\ServisTable' => function($sm) {
                    $tableGateway = $sm->get('ServisTableGateway');
                    $table = new ServisTable($tableGateway);
                    return $table;
                },
                'ServisTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Servis());
                    return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);
                },
                        
                'Servis\Model\CategoryTable' => function($sm) {
                    $tableGateway = $sm->get('CategoryTableGateway');
                    $table = new CategoryTable($tableGateway);
                    return $table;
                },
                'CategoryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Category());
                    return new TableGateway('category', $dbAdapter, null, $resultSetPrototype);
                },
          
                        
            ),
        );
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

}