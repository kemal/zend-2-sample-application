<?php

namespace Servis\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Servis\Model\Servis;          // <-- Add this import
use Servis\Model\Category;          // <-- Add this import
use Servis\Form\ServisForm;       // <-- Add this import
use Servis\Service\CategoryService;

class ServisController extends AbstractActionController {

    protected $servisTable;
    protected $categoryTable;
     
    public function getServisTable() {
        if (!$this->servisTable) {
            $sm = $this->getServiceLocator();

            $this->servisTable = $sm->get('Servis\Model\ServisTable');
        }
        return $this->servisTable;
    }

    public function getCategoryTable() {
        if (!$this->categoryTable) {
            $sm = $this->getServiceLocator();
            $this->categoryTable = $sm->get('Servis\Model\CategoryTable');
        }
        return $this->categoryTable;
    }
 

    // Add content to this method:
    public function addAction() {
        $form = new ServisForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $album = new Servis();
            $form->setInputFilter($album->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $album->exchangeArray($form->getData());
                $this->getServisTable()->saveServis($album);

                // Redirect to list of albums
                return $this->redirect()->toRoute('album');
            }
        }
        return array('form' => $form);
    }

    public function indexAction() {
        return new ViewModel(array(
                    'albums' => $this->getServisTable()->fetchAll(),
                ));
    }

    public function categoryAction() {

        $categories = $this->getCategoryTable()->fetchAll();

        $view = new ViewModel(array(
                    'categories' => $categories,
                ));
        //$view->setTemplate('category');

        return $view;
    }

    public function categoriesAction() {
         $tresults = $this->getServiceLocator()->get('category_service')->getCategoriesWithAlbums();
         return new ViewModel(array(
                    'results' => $tresults,
                ));
        
    }

    public function editAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('album', array(
                        'action' => 'add'
                    ));
        }
        $album = $this->getServisTable()->getServis($id);

        $form = new ServisForm();
        $form->bind($album);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($album->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getServisTable()->saveServis($form->getData());

                // Redirect to list of albums
                return $this->redirect()->toRoute('album');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('album');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getServisTable()->deleteServis($id);
            }

            // Redirect to list of albums
            return $this->redirect()->toRoute('album');
        }

        return array(
            'id' => $id,
            'album' => $this->getServisTable()->getServis($id)
        );
    }

}